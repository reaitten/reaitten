<a href="https://t.me/re32u">
  <img src="https://img.shields.io/badge/@re32u-blue?style=social&logo=Telegram&theme=dark"/></a> 

<a href="https://github.com/reaitten">
  <img src="https://img.shields.io/badge/@reaitten-white?style=social&logo=github&theme=dark"/></a>

<a href="https://hits.seeyoufarm.com"><img src="https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgitlab.com%2Freaitten%2Freaitten&count_bg=%231D3A96&title_bg=%23000000&icon=&icon_color=%23E7E7E7&title=hits&edge_flat=false"/></a>

![reaitten's gitlab stats](https://gitlab-readme-stats.vercel.app/api?username=reaitten&show_icons=true&theme=dark&count_private=true)  

<!-- BLOG-POST-LIST:START -->
<!-- BLOG-POST-LIST:END -->
